<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('register', 'Auth\RegisterController@store');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::middleware('auth:api')->group(function () {
        Route::get('me', 'UserController@me');
    });
});

Route::middleware('auth:api')->group(function () {
    Route::apiResource('room', 'RoomController');
    Route::apiResource('schedule', 'ScheduleController');
    Route::apiResource('meeting', 'MeetingController');
    Route::post('meeting/available', 'MeetingController@getPeopleTime');
});
