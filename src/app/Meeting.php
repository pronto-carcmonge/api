<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'duration',
        'room_id',
        'user_id',
    ];

    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function duration($date, $time)
    {
        $time = explode(':', $time);
        $time = ($time[0]*60) + ($time[1]) + ($time[2]/60);
        $result = Carbon::parse($date)->addMinutes($time)->format("Y-m-d H:i:s");
        return  $result;
    }
}
