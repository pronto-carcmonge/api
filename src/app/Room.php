<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }

    public function schedules()
    {
        return $this->belongsToMany('App\Schedule', 'rooms_schedules');
    }
}
