<?php

namespace App\Http\Controllers;

use App\Room;
use App\User;
use DateTime;
use DatePeriod;
use App\Meeting;
use DateInterval;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\MeetingStore;
use App\Http\Requests\MeetingUpdate;
use App\Http\Resources\MeetingResource;

class MeetingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meetings = Meeting::all();

        return MeetingResource::collection($meetings);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Meeting $meeting)
    {
        return new MeetingResource($meeting);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\MeetingStore  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeetingStore $request)
    {
        $time = explode(':', $request->duration);
        $time = ($time[0]*60) + ($time[1]) + ($time[2]/60);
        $date_finish = Carbon::parse($request->date)->addMinutes($time)->format("Y-m-d H:i:s");

        $meeting = Meeting::create([
            'date' => $request->date,
            'duration' => $date_finish,
            'room_id' => $request->room_id,
            'user_id' => $request->user_id,
        ]);

        return new MeetingResource($meeting);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MeetingUpdate $request, Meeting $meeting)
    {
        $request->has('date') ? $meeting->date = $request->date : null;
        $request->has('duration') ? $meeting->duration = $request->duration : null;
        $request->has('room_id') ? $meeting->room_id = $request->room_id : null;
        $request->has('user_id') ? $meeting->user_id = $request->user_id : null;

        if (!$meeting->isDirty()) {
            return $this->errorResponse('Specify at least one different value to update', 422);
        }

        $meeting->save();

        return new MeetingResource($meeting);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meeting $meeting)
    {
        $meeting->delete();

        return new MeetingResource($meeting);
    }

    public function getPeopleTime()
    {
        $schedules = Room::find(1)->schedules->toArray();
        $user = User::all();

        $new_schedules = [];
        
        foreach ($schedules as $key => $value) {
            $period = new DatePeriod(
                new DateTime($value["time_start"]),
                new DateInterval('PT30M'),
                new DateTime($value["time_finish"])
            );
    
            foreach ($period as $key_hours => $value_hours) {
                $time = $value_hours->format('H:i:s');
                $users_not_available = [];
                $users_available = [];
    
                $user->each(function ($user) use (&$users_not_available, &$users_available, $time) {
                    $user->meetings->each(function ($meeting) use (&$users_not_available, &$users_available, $time, $user) {
                        $time_meeting = Carbon::parse($meeting->date)->format("H:i:s");
                        unset($user->meetings);
    
                        if ($time == $time_meeting) {
                            $users_not_available[] = $user->first_name;
                        } else {
                            $users_available[] = $user->first_name;
                        }
                    });
                });
                $users_not_available = array_values(array_unique($users_not_available));
                $users_available = array_values(array_unique(array_diff($users_available, ($users_not_available))));
                $new_schedules[] = [
                    "hour" => $value_hours->format('H:i:s'),
                    "users_not_available" => $users_not_available,
                    "users_available" => $users_available,
                ];
            }
        }


        return $new_schedules;
    }

    // public static function betweenHour($currentTime, $startTime, $endTime)
    // {
    //     $currentTime = (new DateTime($currentTime));
    //     $startTime = new DateTime($startTime);
    //     $endTime = new DateTime($endTime);
    //     $endTime->modify('-1 minutes');
        
    //     if ($currentTime >= $startTime && $currentTime <= $endTime) {
    //         $res = true;
    //     } else {
    //         $res = false;
    //     }
    //     return $res;
    // }
}
