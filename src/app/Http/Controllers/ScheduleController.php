<?php

namespace App\Http\Controllers;

use App\Schedule;
use Illuminate\Http\Request;
use App\Http\Requests\ScheduleStoreRequest;
use App\Http\Requests\ScheduleUpdate;
use App\Http\Resources\ScheduleResource;

class ScheduleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::all();

        return ScheduleResource::collection($schedules);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        return new ScheduleResource($schedule);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ScheduleStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScheduleStoreRequest $request)
    {
        $fields = $request->all();

        $schedule = Schedule::create([
            'time_start' => $request->time_start,
            'time_finish' => $request->time_finish,
        ]);

        return new ScheduleResource($schedule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ScheduleUpdate $request, Schedule $schedule)
    {
        $request->has('time_start') ? $schedule->time_start = $request->time_start : null;
        $request->has('time_finish') ? $schedule->time_finish = $request->time_finish : null;

        if (!$schedule->isDirty()) {
            return $this->errorResponse('Specify at least one different value to update', 422);
        }

        $schedule->save();

        return new ScheduleResource($schedule);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $schedule->delete();

        return new ScheduleResource($schedule);
    }
}
