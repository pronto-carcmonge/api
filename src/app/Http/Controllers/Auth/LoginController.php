<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    private $guard;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a Token via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $this->guard = Auth::guard('api');

        $credentials = request(['email', 'password']);
        $token = $this->guard->attempt($credentials);

        if (!$token) {
            return response([
                'message' => 'Invalid credentials',
            ], 409);
        }

        return [
            'user' => auth('api')->user(),
            'token' => $token,
            'token_type' => 'Bearer',
        ];
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        Auth::logout();
        return response()->json(['Message' => 'Session close']);
    }
}
