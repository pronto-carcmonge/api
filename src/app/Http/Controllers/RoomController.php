<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;
use App\Http\Requests\RoomStore;
use App\Http\Requests\RoomUpdate;
use App\Http\Resources\RoomResource;

class RoomController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::all();

        return RoomResource::collection($rooms);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        return new RoomResource($room);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\RoomStore  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoomStore $request)
    {
        $fields = $request->all();

        $room = Room::create([
            'name' => $request->name,
        ]);

        return new RoomResource($room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoomUpdate $request, Room $room)
    {
        $request->has('name') ? $room->name = $request->name : null;

        if (!$room->isDirty()) {
            return $this->errorResponse('Specify at least one different value to update', 422);
        }

        $room->save();

        return new RoomResource($room);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $room->delete();

        return new RoomResource($room);
    }
}
