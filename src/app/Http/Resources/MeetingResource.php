<?php

namespace App\Http\Resources;

use App\Http\Resources\RoomResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MeetingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'date'    => $this->date,
            'duration'    => $this->duration,
            'user'    => new UserResource($this->user),
            'room'    => new RoomResource($this->room),
        ];
    }
}
